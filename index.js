console.log('Hello');

// yesterday lesson review


/*function	addNumber(number){
	let sum = number + 8;
	console.log("the sum is "+sum);
}

addNumber(8);
*/

/*let greet = 'Good Morning';
function greetName(){
	let name = prompt('What is your name?');
	alert(greet+' '+name);
}*/

// greetName();

// todays lesson
// return

/*function subNum(x,y){
	let total = x - y;
	return total;
}

console.log(subNum(4,2));*/

// while loop

/*function count(number){
	while (number > 0) {
		console.log(number);
		number = number -1;
	} 
	console.log(number);		
}

count(6);
*/

// do while loop

/*let antiInfiniteLoop=0;

function count2(number){
	do {
			console.log(number);
			number = number - 1;
			antiInfiniteLoop = antiInfiniteLoop + 1;
			if (antiInfiniteLoop > 1000) {
				console.log('antiInfiniteLoop triggered')
				break;
				}
		} while (number > 0)
		console.log(number);
	
}

count2(150);*/

// FUNCTIONS PARAMETERS & RETURNS

// arguments are passed data
// parameters accepts data

// multiple arguments are accepted based on order it was given

// A parameter is a variable in a method definition. When a method is called, the arguments are the data you pass into the method's parameters.

// RETURNS STATEMENTS

// return executes the last statement and exits the function
// return forward the value and exit the function

// console.log only displays but doesn't forward value

// console.clear();

// Circle area = π * r² = π * 225 [cm²]




// Function parameters are variables that wait for a value from an argument in the function invocation
function sayMyName(name){
	console.log('My name is '+name)
}

// Function arguments are values like strings, numbers, etc. that you can pass onto the function you are invoking
sayMyName('Bernard');


// You can re-use function by invoking them at any time or any part of your code. Make sure you have declared them first.
sayMyName('vice ganda');

let myName = 'blue';
sayMyName(myName);


// You can use arguments and parameters to make the data inside your function dynamic
function sumOfTwoNumbers(firstNumber, secondNumber){
	let sum = 0;

	sum = firstNumber + secondNumber;

	console.log(sum);
}

sumOfTwoNumbers(10, 15);
sumOfTwoNumbers(100, 150);

console.clear();


// you can pass a function as an argument for another function. Do not pass that function with parentheses, only with its function name
function argumentFunction(){
	console.log('This is a function that is passed as an argument.')
}

function parameterFunction(argumentFunction){
	argumentFunction();
}

parameterFunction(argumentFunction);

// REAL WORLD APPLICATION 
/*
	Imagine a product page where you have an 'add to cart' button wherein you have the button itself displayed but you dont have the functionality of it yet. This is where passing a function as an argument comes in, in order for you to add or have access to a function to add functionality to your button once it is clicked
*/
function addProductToCart(){
	console.log('Click me to add product to cart');
}
let addToCartBtn = document.querySelector('#add-to-cart-btn')

addToCartBtn.addEventListener('click', addProductToCart)

// If you add extra or if you lack an argument, javascript won't throw an error at you, instead it will ignore the extra argument and turn the lacking parameters into undefined
function displayFullname(firstName, middleInitial, lastName){
	console.log('Your full name is: '+ firstName +' '+ middleInitial +' '+ lastName);
}

displayFullname('jeff', 'n', 'carpio');
displayFullname('Johnny', 'B', 'Goode', 'Yeah');



// STRING INTERPOLATION
/*function displayMovieDetails(title, synopsis, director){
	console.log(`The movie is titled ${title}`)
	console.log(`The movie is synopsis ${synopsis}`)
	console.log(`The movie is director ${director}`)
}

displayMovieDetails('batman', 'i forgot', 'bug')*/



// console.clear();

/*function isPassing(score,numberOfItems){
	console.log('Is '+a+'/'+b+' a passing score?');
	let percentToGet = 50;
	let percent = (percentToGet / 100) * score;
	console.log(percentToGet + "% of " + number + " is " + percent);
	let pass = numberOfItems * .7;
	return score > pass;
	
}

console.log(isPassing(38,50));*/

// you can use double quotation OR backticks to accomodate strings that have apostrophies
// console.log('')


// Return statement/syntax is the final step of any function, It assigns whatever value you put after it as the value of the function upon invoking it
/*
	Note:
	1. Return statements should always come last in the function scope
	2. Any statement after the return statement will be ingored by the function

*/ 
function displayPlayerDetails(name, age, playerclass){

	let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerclass}`;

	return playerDetails;

	console.log('Hello');

}

console.log(displayPlayerDetails('Merlin', 28, 'Mage'));

